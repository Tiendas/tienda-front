'use strict';

/**
 * @ngdoc function
 * @name tiendaFrontApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the tiendaFrontApp
 */
angular.module('tiendaFrontApp')
  .controller('AboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
